(defparameter *lis* nil)

;property von x aendern
(defun change (x a)
  (setf (get x 'value) a)
)

;symbol *lis* hinzufuegen
(defun add (val)
  (let
    (
      (new-element (+ 1 (length *lis*)))
    )
    (setf (get 'new-element 'value) val)
    (setq *lis* (cons 'new-element *lis*))
  )
)

;*lis* ausgeben
(defun p ()
  (print (mapcar 'symbol-plist *lis*))
)

(add 1)
(p)
(add 2) ;die liste SOLL hier ((value 2) (value 1)) aber ist ((value 2) (value2))
(p)
(change (first *lis*) 3)
(p)
(add 5)
(p)
